<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/article', 'ArticleController@create');
Route::post('/article', 'ArticleController@store');

Route::get('/articles/{userId}', 'ArticleController@userArticles');

Route::get('/article/{id}/like/toggle', 'ArticleController@toggleLike');

Route::get('/comment', 'CommentController@create');
Route::post('/comment', 'CommentController@store');

Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

Route::post('/create', 'CreateController@store');
Route::get('/create', 'CreateController@create');

Route::get('/', 'IndexController@create');
// Route::get('/', function () {
//
//     $faker = Factory::create();
//     $headline = News::all();
//
//
//     $card1 = new lowerContent();
//     $card1->image = $faker->imageURL();
//     $card1->title = $faker->text(60);
//     $card1->source = $faker->text(5);
//
//
//     $card2 = new lowerContent();
//     $card2->image = $faker->imageURL();
//     $card2->title = $faker->text(60);
//     $card2->source = $faker->text(5);
//
//     $card3 = new lowerContent();
//     $card3->image = $faker->imageURL();
//     $card3->title = $faker->text(60);
//     $card3->source = $faker->text(5);
//
//     $card4 = new lowerContent();
//     $card4->image = $faker->imageURL();
//     $card4->title = $faker->text(60);
//     $card4->source = $faker->text(5);
//
//     $card5 = new lowerContent();
//     $card5->image = $faker->imageURL();
//     $card5->title = $faker->text(60);
//     $card5->source = $faker->text(5);
//
//     $card6 = new lowerContent();
//     $card6->image = $faker->imageURL();
//     $card6->title = $faker->text(60);
//     $card6->source = $faker->text(5);
//
//
//
//     $data = [
//         'headline' => $headline,
//         'cards' => [$card1, $card2, $card3, $card4, $card5, $card6],
//     ];
//     return view('index', $data);
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
