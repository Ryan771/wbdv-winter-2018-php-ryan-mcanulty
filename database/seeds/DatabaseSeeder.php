<?php

use Illuminate\Database\Seeder;
use App\Models\News;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $headline = new News();
        $headline->image=$faker->imageURL();
        $headline->title=$faker->text(75);
        $headline->content = $faker->text(180);
        $headline->source = $faker->text(10);
        $headline->save(); 
    }
}
