<?php

namespace App;
use App\User;
use App\Models\Comment;
use App\Models\Article;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public function article()
    {
        return $this->hasMany(Article::class);
    }

    public function comment(){
        return $this->hasMany(Comment::class);
    }




    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
