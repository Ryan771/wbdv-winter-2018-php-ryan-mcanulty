<?php

namespace App\Models;
use App\Models\Comment;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes(){
        return $this->belongsToMany(User::class);
    }

    public function isLikedByCurrentUser(){
        $currentUser = request()->user();
        return $this->likes->contains($currentUser);
    }
}
