<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Article;

class Comment extends Model
{
    public function article(){
        $this->hasMany(Article::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
