<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function create(){
        return view('contact', [

        ]);
    }

    public function store(){

        $request = request();

        $result = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'contact' => 'required'
        ],  [
            'name.required'=> 'Please enter your name',
            'email.required'=> 'Please enter a valid email',
            'contact.required'=> 'Please include a message'
        ]);

        $data = request()->all();

        

        ##dd($data);
        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->email = $data['email'];
        $contact->content = $data['contact'];
        $contact->save();

        return redirect('/contact')->with('message', 'Your contact was successfully processed.
        Someone will be in touch with you soon.');
    }
}
