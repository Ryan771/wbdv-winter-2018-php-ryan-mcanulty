<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\User;

class ArticleController extends Controller
{
    public function create(){
        if (!\Auth::check()) {
        return redirect('/login');
    }
        return view('/create');
    }

    public function store(){
        $request = request();

        $result = $request->validate([
            'title' => 'required',
            'content' => 'required'
        ],  [
            'title.required'=> 'Please enter a title',
            'content.required'=> 'Please include a message'
        ]);
        $loggedInUser = $request->user();
        $data = request()->all();

        $article = new Article();
        $article->user_id = $loggedInUser->id;
        $article->title = $data['title'];
        $article->content = $data['content'];
        $article->save();

        return redirect('/');
    }

    public function userArticles($userId){
        $user = User::where('name', $userId)
        ->orWhere('id',$userId)
        ->first();

        if(!$user){
            abort(404);
        }

        $articles=$user->article;
        return view('userArticles',[
            'user'=>$user,
            'articles'=>$articles
        ]);
    }

    public function toggleLike($articleId)
    {
    $user = request()->user();
    $article = Article::find($articleId);

    if ($article->isLikedByCurrentUser()) {
        $article->likes()->detach($user);
    } else {
        $article->likes()->attach($user);
    }

    return back()
        ->with('message', 'You successfully liked an article!');
    }

}
