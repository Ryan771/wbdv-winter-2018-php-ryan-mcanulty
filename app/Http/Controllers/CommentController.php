<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Comment;
use App\Models\Article;

class CommentController extends Controller
{
    public function create() {

       if (!\Auth::check()) {
           return redirect('/login');
       }
       return view('/');
   }

   public function store() {
       if (!\Auth::check()) {
           return redirect('/login');
       }


      $request = request();

      $result = $request->validate([
          'content' => 'required|max:255'
      ],  [
          'content.required' => 'Please enter a comment',
      ]);
      $loggedInUser = $request->user();
      $data = request()->all();

      $comment = new Comment();
      $comment->user_id = $loggedInUser->id;
      $comment->article_id = $data['articleId'];
      $comment->content = $data['content'];
      $comment->save();

    return redirect('/')->with('message', 'Your Comment was successfully posted!');
   }
}
