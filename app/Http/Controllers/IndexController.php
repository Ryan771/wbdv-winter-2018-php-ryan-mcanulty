<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Article;
use App\Models\Comment;

class IndexController extends Controller
{
    public function create() {
        $users = User::all();

        $articles = Article::all();

        $comments = Comment::all();

        $data = [
            'users' => $users,
            'articles' => $articles,
            'comments' => $comments
        ];

        return view('index', $data);
    }
}
