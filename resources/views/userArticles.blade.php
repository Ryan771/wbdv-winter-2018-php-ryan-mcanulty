@extends('layout')

@section('content')


    <div class="container">
        <h1> {{$user->name}}</h1>
        <div class="row">

            @foreach($articles as $article)
                <div class="col-md-6">
                    <div class="article-container">
                        <div class="content-container">
                        <ul>
                            <br/>
                            <li> <h2><strong>{{$article->title}}</strong></h2></li>
                            <li>{{$article->content}}</li>
                        </ul>
                    </div>
                        <hr>

                <span class="comments">Comments:</span><br/>
                    <?php foreach($article->comment as $comment) :?>
                        <ul>
                            <li><?php echo $comment->content?><br/><br/></li>
                            <li class="time"><?php echo $comment->updated_at->diffForHumans()?><br/></li>
                            <li class="userComment">    User: <?php echo $comment->user->name ?><br/></li>
                            <hr>
                        </ul>
                    <?php endforeach; ?>

                <form method="post" action="/comment">
                    <?php echo csrf_field()?>
                        <div class="comment-input">
                            <textarea placeholder="Add a comment" rows="1" cols="20" name="content"></textarea><br/>
                            <input type="submit" value="comment" width="100px"><br/>
                            <input type="hidden" name="articleId" value="{{$article->id}}">
                            @if ($article->isLikedByCurrentUser())
                                <span><a href="/article/{{ $article->id }}/like/toggle">Unlike</a></span>
                            @else
                                <span><a href="/article/{{ $article->id }}/like/toggle">Like</a></span>
                            @endif
                            <br/>
                            <br/>
                            Posted by: <a href="/articles/{{$article->user->id}}"><span>{{$article->user->name}}</span></a>
                        </div>
                    </div>
                    <br/>
                </div>
            @endforeach
        </div>
    </div>

@endsection
