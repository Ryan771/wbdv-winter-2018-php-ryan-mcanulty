<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SAIT Winter 2018</title>
        <link rel="stylesheet" href="/css/app.css?<?php echo time();?>">
    </head>

    <header>

        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light ">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a href="" class="logo"><img src="https://i.cbc.ca/1.4066392.1500303304!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_620/cbc-logo-horizontal.jpg"></a>
                    </li>
                    <li class ="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                        <li class ="nav-item active">
                        <a class="nav-link" href="/article">Create an Article <span class="sr-only">(current)</span></a>
                    </li>
                    </li>
                        <li class ="nav-item active">
                        <a class="nav-link" href="/contact">Contact Us<span class="sr-only">(current)</span></a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li>
                        @if (Auth::check())
                            Hello, {{ Auth::user()->name }}
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                           Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a href="/login">Login</a>
                            <a href="/register">Register</a>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <body>
        <?php if($errors->any()): ?>
            <div class = "alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error):?>
                        <li>
                            <?php echo $error?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif ?>
        @yield('content')
    </body>
</html>
