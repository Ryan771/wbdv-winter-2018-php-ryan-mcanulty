<div class="form-group">
    <label for="<?php echo $name ?>"><?php echo $label ?></label>
    <input type="text" name="<?php echo $name ?>" placeholder="<?php echo $label ?>" value="<?php echo old($name) ?>" class="form-control <?php echo $errors->has($name) ? 'is-invalid' : '' ?>">
    <?php if($errors->has($name)): ?>
        <span class="invalid-feedback"><?php echo $errors->first($name) ?>
    <?php endif; ?>
</div>
