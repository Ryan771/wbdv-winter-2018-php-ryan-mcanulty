@extends('layout')
@section('content')
    <?php if($message = session('message')): ?>
        <div class="alert alert-success">
            <?php echo $message?>
        </div>
    <?php endif; ?>

<div class="container">
    <h1>Contact Form</h1>
    <form method="post">
        <?php echo csrf_field()?>

        @include('forms.text', [
            'label'=>'Name',
            'name'=>'name'
        ])

        @include('forms.text', [
            'label'=> 'Email',
            'name'=>'email'
        ])

        @include('forms.text',[
            'label'=>'Message',
            'name' => 'contact'
        ])
        <input type="submit" name="" value="Submit">
    </form>
</div>

@endsection
