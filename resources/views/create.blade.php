@extends('layout')
@section('content')
<div class="container">

<h1>Create an Article</h1>

<?php if($errors->any()): ?>
    <div class = "alert alert-danger">
        <ul>
            <?php foreach ($errors->all() as $error):?>
                <li>
                    <?php echo $error?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif ?>


    <form method="POST" action="/article">
        <?php echo csrf_field()?>
        <label for="title">Title</label><br/>
        <input type="text" name="title" /> <br/>
        <label for="content">Content</label><br/>
        <textarea rows="5" cols="50" name="content"></textarea><br/>
        <input type="submit" value="submit" />
    </form>


@endsection
